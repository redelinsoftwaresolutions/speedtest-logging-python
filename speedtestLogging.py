#!/usr/bin/python

# install speedtest-cli: https://linuxundich.de/gnu-linux/speedtest-net-speedtest-cli-im-terminal-ohne-browser/

import subprocess
from datetime import datetime
import os
import os.path

pathToFile='/tmp/speedtestStatistic.csv'

def initFile():
	tmpFile=''	
	if (not os.path.isfile(pathToFile)):
		header="datetime;ping (ms);download (Mbit/s);upload (Mbit/s); \r\n"
		tmpFile=open(pathToFile, "w")
		tmpFile.write(header)
	else:
		tmpFile=open(pathToFile, "a")
	return tmpFile	

file=initFile()

def run_command(command):
    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    return iter(p.stdout.readline, b'')

outLine=str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) + ";"
command = '/usr/local/bin/speedtest --simple'.split()
for line in run_command(command):
	outLine=outLine + line.replace('\n', '').replace('Ping: ', '').replace('Download: ', '').replace('Upload: ', '').replace(' Mbit/s', '').replace(' ms', '').replace('.', ',') + ";"

file.write(outLine + "\r\n")

