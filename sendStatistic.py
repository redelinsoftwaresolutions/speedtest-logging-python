#!/usr/bin/python

import os
import socket
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

smtpserver = os.environ.get('SPEEDTEST_MAIL_SMTP')
emailfrom = os.environ.get('SPEEDTEST_MAIL_FROM')
emailfromname = os.environ.get('SPEEDTEST_MAIL_FROM_NAME')
emailto = os.environ.get('SPEEDTEST_MAIL_TO')
fileToSend = "/tmp/speedtestStatistic.csv"
username = os.environ.get('SPEEDTEST_MAIL_FROM')
password = os.environ.get('SPEEDTEST_MAIL_FROM_PWD')

msg = MIMEMultipart()
msg["From"] = "%s <%s>" % (emailfromname, emailfrom)
msg["To"] = emailto
msg["Subject"] = "Speedtest-Statistic from %s (%s)" % (emailfromname, socket.gethostname()) 
msg.preamble = "Speedtest-Statistic from %s (%s)" % (emailfromname, socket.gethostname())

ctype, encoding = mimetypes.guess_type(fileToSend)
if ctype is None or encoding is not None:
    ctype = "application/octet-stream"

maintype, subtype = ctype.split("/", 1)

if maintype == "text":
    fp = open(fileToSend)
    # Note: we should handle calculating the charset
    attachment = MIMEText(fp.read(), _subtype=subtype)
    fp.close()
elif maintype == "image":
    fp = open(fileToSend, "rb")
    attachment = MIMEImage(fp.read(), _subtype=subtype)
    fp.close()
elif maintype == "audio":
    fp = open(fileToSend, "rb")
    attachment = MIMEAudio(fp.read(), _subtype=subtype)
    fp.close()
else:
    fp = open(fileToSend, "rb")
    attachment = MIMEBase(maintype, subtype)
    attachment.set_payload(fp.read())
    fp.close()
    encoders.encode_base64(attachment)
attachment.add_header("Content-Disposition", "attachment", filename=fileToSend)
msg.attach(attachment)

server = smtplib.SMTP(smtpserver) #normally you have to use tls port as well
server.starttls()
server.login(username,password)
server.sendmail(emailfrom, emailto, msg.as_string())
server.quit()
